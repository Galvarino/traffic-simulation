# Traffic Simulation using ThreeJS
This is a project that aims to simulate the traffic of a city using ThreeJS. 

## Instructions
The project has been initialised using Vite. To set the project up for development, clone the repository, install the dependencies and run the development start command.

```
git clone https://gitlab.com/Galvarino/traffic-simulation
cd traffic-simulation
npm install
npm run dev
```
