import * as THREE from "three";
import Lane from "../components/Lane";
import { coord, lane, laneWithCoords } from "../types/roads";
import Junction from "../components/Junction";

export const mapGenerator = (laneList: lane[], junctionCoords: coord[]): { layout: THREE.Group, junctions: Junction[] } => {
  const layout = new THREE.Group();

  // building lanes array with coords
  const lanes: laneWithCoords[] = laneList.map(lane => {
    return { p1: junctionCoords[lane.p1], p2: junctionCoords[lane.p2] }
  });

  // adding lanes
  lanes.forEach(lane => {
    if (lane.p1.x === lane.p2.x) {
      if (lane.p1.z > lane.p2.z) {
        layout.add(new Lane({ x: lane.p1.x, z: lane.p1.z - 80 }, { x: lane.p2.x, z: lane.p2.z + 80 }).getLane())
      }
      else {
        layout.add(new Lane({ x: lane.p1.x, z: lane.p1.z + 80 }, { x: lane.p2.x, z: lane.p2.z - 80 }).getLane())
      }
    }
    else if (lane.p1.z === lane.p2.z) {
      if (lane.p1.x > lane.p2.x) {
        layout.add(new Lane({ x: lane.p1.x - 80, z: lane.p1.z }, { x: lane.p2.x + 80, z: lane.p2.z }).getLane())
      }
      else {
        layout.add(new Lane({ x: lane.p1.x + 80, z: lane.p1.z }, { x: lane.p2.x - 80, z: lane.p2.z }).getLane())
      }
    }
    else {
      throw new Error("Ensure either x or z is equal (lane cannot have bends)");
    }
  })

  // adding junctions
  const junctions: Junction[] = []
  junctionCoords.forEach((coord) => {
    const junction: Junction = new Junction(coord);
    layout.add(junction.getJunction())
    junctions.push(junction);
  })
  return { layout, junctions };
}