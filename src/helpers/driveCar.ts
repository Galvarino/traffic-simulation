import Car from "../components/Car";
import Junction from "../components/Junction";

export default function driveCar(car: Car, speed: number, junctions: Junction[],  cars: Car[]) {
  if(!car.spawn(junctions, cars)) {
    return;
  }
  if (!car.journeyEnded) {
    if (car.waitingAtJunction) {
      if (!car.turnPhase1Completed) {
        car.turnPhase1(speed, junctions);
      }
      else if (!car.turnPhase2Completed) {
        car.turnPhase2(junctions);
      }
    }
    else {
      car.moveStraight(speed, junctions, cars);
    }
  }
}