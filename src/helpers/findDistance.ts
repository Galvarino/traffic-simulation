import { coord } from "../types/roads";

export default function findDistance(c1: coord, c2: coord) {
	return Math.abs(c1.x - c2.x) + Math.abs(c1.z - c2.z)
}