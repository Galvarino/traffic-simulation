import Car from "../components/Car";
import { Direction } from "../enums/direction";
import { coord } from "../types/roads";
import findDistance from "./findDistance";

function oppositeDirections(direction1: Direction, direction2: Direction): boolean {
  if (direction1 === Direction.North && direction2 === Direction.South) {
    return true;
  }
  if (direction1 === Direction.South && direction2 === Direction.North) {
    return true;
  }
  if (direction1 === Direction.East && direction2 === Direction.West) {
    return true;
  }
  if (direction1 === Direction.West && direction2 === Direction.East) {
    return true;
  }
  return false;
}

export default function isSafePair(car1: Car, car2: Car): boolean {
  if (car1.carIndex === car2.carIndex) {
    return true;
  }
  const direction1 = car1.direction
  const direction2 = car2.direction
  const coord1: coord = { x: car1.car.position.x, z: car1.car.position.z }
  const coord2: coord = { x: car2.car.position.x, z: car2.car.position.z }
  if (direction1 !== direction2) {
    return true;
  }
  const distance = findDistance(coord1, coord2);
  console.log(distance, car1.carIndex)
  if (distance > 50) {
    return true;
  }
  if (car2.spawned === false) {
    return true;
  }
  switch (direction1) {
    case Direction.North:
      if (coord1.z < coord2.z) {
        return true;
      }
      break;
    case Direction.East:
      if (coord1.x > coord2.x) {
        return true;
      }
      break;
    case Direction.South:
      if (coord1.z > coord2.z) {
        return true;
      }
      break;
    case Direction.West:
      if (coord1.x < coord2.x) {
        return true;
      }
      break;
  }
  return false;
}