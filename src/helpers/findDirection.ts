import { Direction } from "../enums/direction";
import { coord } from "../types/roads";

export default function findDirection(firstCoord: coord, secondCoord: coord): Direction {
  if (firstCoord.x === secondCoord.x) {
    if (firstCoord.z > secondCoord.z) {
      return Direction.North;
    }
    else {
      return Direction.South;
    }
  }
  else if (firstCoord.z === secondCoord.z) {
    if (firstCoord.x > secondCoord.x) {
      return Direction.West;
    }
    else {
      return Direction.East;
    }
  }
  throw new Error("not valid coords")
}