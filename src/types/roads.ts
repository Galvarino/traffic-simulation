export type coord = {
  x: number;
  z: number;
};

export type lane = {
  p1: number;
  p2: number;
}

export type laneWithCoords = {
  p1: coord;
  p2: coord;
}