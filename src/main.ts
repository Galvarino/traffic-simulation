import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import Car from "./components/Car";
import { coord, lane } from "./types/roads";
import { mapGenerator } from "./helpers/mapGenerator";
import driveCar from "./helpers/driveCar";
import "./style.css";

// Creating Scene
const scene = new THREE.Scene();

// Lights
const ambientLight = new THREE.AmbientLight(0xffffff, 0.6);
scene.add(ambientLight);

const dirLight = new THREE.DirectionalLight(0xffffff, 1);
dirLight.position.set(-50, 50, 100);
scene.add(dirLight);

// Camera
const aspectRatio = window.innerWidth / window.innerHeight;
const cameraWidth = window.innerWidth;
const cameraHeight = cameraWidth / aspectRatio;

const camera = new THREE.OrthographicCamera(
  cameraWidth / -2, //left
  cameraWidth / 2, //right
  cameraHeight / 2, //top
  cameraHeight / -2, //bottom
  0, // near plane
  1000 // far plane
);
camera.position.set(200, 200, 200);
camera.up.set(0, 1, 0);
camera.lookAt(0, 0, 0);

// creating the junction coords
const junctionCoords: coord[] = [
  { x: -70, z: 30 },
  { x: -310, z: 30 },
  { x: -70, z: -210 },
  { x: -70, z: 350 },
  { x: -310, z: 350 },
];

// creating a map
const paths: lane[] = [
  { p1: 0, p2: 1 },
  { p1: 0, p2: 2 },
  { p1: 0, p2: 3 },
  { p1: 3, p2: 4 },
];

const map = mapGenerator(paths, junctionCoords);

scene.add(map.layout);
map.junctions[0].oscillate(5000, 2500);
map.junctions[1].oscillate(5000);
map.junctions[2].oscillate(5000);
map.junctions[3].oscillate(5000, 2500);
map.junctions[4].oscillate(5000, 2500);

const path1 = [1, 0, 2];
const car1 = new Car(0, path1, junctionCoords, 0x9900ff);
scene.add(car1.getCar());

const car2 = new Car(1, path1, junctionCoords, 0x9900ff);
scene.add(car2.getCar());

const path2 = [3, 0, 1];
const car3 = new Car(2, path2, junctionCoords, 0xff0000);
scene.add(car3.getCar());

const path3 = [1, 0, 3, 4];
const car4 = new Car(3, path3, junctionCoords, 0xffff00);
scene.add(car4.getCar());

// Renderer
const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.render(scene, camera);

// Orbit Controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.update();

// Axes Helper
const axesHelper = new THREE.AxesHelper(5);
axesHelper.position.set(0, 0, 0);
scene.add(axesHelper);

// Resize
window.addEventListener("resize", onWindowResize, false);
function onWindowResize() {
  // camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
  render();
}

const cars: Car[] = [car1, car2, car3, car4];
console.log(cars);

// Animate
function animate() {
  controls.update();
  renderer.render(scene, camera);
  requestAnimationFrame(animate);
  driveCar(car1, 0.5, map.junctions, cars);
  driveCar(car2, 0.5, map.junctions, cars);
  driveCar(car3, 0.5, map.junctions, cars);
  driveCar(car4, 1, map.junctions, cars);
  render();
}

function render() {
  renderer.render(scene, camera);
}

animate();

document.body.appendChild(renderer.domElement);
