import * as THREE from "three";
import { coord } from "../types/roads";
import Block from "./Block";
import Arrow from "./Arrow";
import { Direction } from "../enums/direction";

export default class Junction {
  allowNorth: boolean;
  allowEast: boolean;
  allowSouth: boolean;
  allowWest: boolean;
  northCoord: coord;
  eastCoord: coord;
  southCoord: coord;
  westCoord: coord;
  junctionCoord: coord;
  junction: THREE.Group;
  arrow: Arrow;
  public constructor(junctionCoord: coord) {
    this.junctionCoord = { ...junctionCoord };
    this.junction = new THREE.Group();
    this.junction.add(new Block(0xff0000).getBlock());
    this.arrow = new Arrow();
    this.junction.add(this.arrow.getArrow());
    this.junction.position.x = junctionCoord.x;
    this.junction.position.z = junctionCoord.z;

    // allow vertical movement initially
    this.allowNorth = true;
    this.allowEast = false;
    this.allowSouth = true;
    this.allowWest = false;
    this.arrow.makeVertical();

    this.northCoord = { x: junctionCoord.x, z: junctionCoord.z - 40 };
    this.eastCoord = { x: junctionCoord.x + 40, z: junctionCoord.z };
    this.southCoord = { x: junctionCoord.x, z: junctionCoord.z + 40 };
    this.westCoord = { x: junctionCoord.x - 40, z: junctionCoord.z };
  }
  public getJunction(): THREE.Group {
    return this.junction;
  }
  public allowVertical(): void {
    this.allowNorth = true;
    this.allowSouth = true;
    this.allowEast = false;
    this.allowWest = false;
    this.arrow.makeVertical();
  }
  public allowHorizontal(): void {
    this.allowNorth = false;
    this.allowSouth = false;
    this.allowEast = true;
    this.allowWest = true;
    this.arrow.makeHorizontal();
  }
  public oscillate(time: number, delay: number = 0) {
    setTimeout(() => {
      this.allowNorth ? this.allowHorizontal() : this.allowVertical();
      setInterval(() => {
        this.allowNorth ? this.allowHorizontal() : this.allowVertical();
      }, time);
    }, delay);
  }
  public isAllowed(d: Direction): boolean {
    switch (d) {
      case Direction.North:
        return this.allowSouth;
      case Direction.East:
        return this.allowWest;
      case Direction.South:
        return this.allowNorth;
      case Direction.West:
        return this.allowEast;
    }
  }
}
