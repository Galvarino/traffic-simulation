import * as THREE from "three";
import { coord } from "../types/roads";

export default class Block {
  block: THREE.Mesh;
  leftIsOccupied: boolean;
  rightIsOccupied: boolean;
  public constructor(color: number, width: number = 80) {
    this.block = new THREE.Mesh(
      new THREE.BoxGeometry(width, 10, width),
      new THREE.MeshLambertMaterial({ color: color })
    );
    this.leftIsOccupied = false;
    this.rightIsOccupied = false;
  }
  public getBlock(): THREE.Mesh {
    return this.block;
  }
  public setPosition(x: number, z: number): void {
    this.block.position.set(x, 0, z);
  }
  public leftIsFree(): boolean {
    return !this.leftIsOccupied;
  }
  public rightIsFree(): boolean {
    return !this.rightIsOccupied;
  }
}