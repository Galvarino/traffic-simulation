import * as THREE from "three";

export default class Arrow {
  arrow: THREE.Mesh;
  public constructor() {
    this.arrow = new THREE.Mesh(
      new THREE.PlaneGeometry(80, 20),
      new THREE.MeshBasicMaterial({ color: 0x00ff00, side: THREE.DoubleSide })
    )
    this.arrow.position.y = 5.1;
    this.arrow.rotation.x = Math.PI / 2
  }
  public getArrow(): THREE.Mesh {
    return this.arrow;
  }
  public makeVertical(): void {
    this.arrow.rotation.z = Math.PI / 2;
  }
  public makeHorizontal(): void {
    this.arrow.rotation.z = 0
  }
}