import * as THREE from "three";
import { coord } from "../types/roads";
import Block from "./Block";


export default class Lane {
  lane: THREE.Group;
  public constructor(start: coord, end: coord, color: number = 0xffffff) {
    if (start.x != end.x && start.z != end.z) {
      throw new Error("Ensure either x or z is equal (lane cannot have bends)");
    }
    if (start.x != end.x && (start.x - end.x) % 80 != 0) {
      throw new Error("Distance must be divisible by 80");
    } else if ((start.z - end.z) % 80 != 0) {
      throw new Error("Distance must be divisible by 80");
    }
    this.lane = new THREE.Group();
    if (start.z === end.z) {
      for (
        let i = 0;
        i < (Math.max(start.x, end.x) - Math.min(start.x, end.x)) / 80 + 1;
        i++
      ) {
        const block = new Block(color);
        block.setPosition(Math.min(start.x, end.x) + 80 * i, start.z);
        this.lane.add(block.getBlock());
      }
    } else {
      for (
        let i = 0;
        i < (Math.max(start.z, end.z) - Math.min(start.z, end.z)) / 80 + 1;
        i++
      ) {
        const block = new Block(color);
        block.setPosition(start.x, Math.min(start.z, end.z) + 80 * i);
        this.lane.add(block.getBlock());
      }
    }
  }
  public getLane(): THREE.Group {
    return this.lane;
  }
}
