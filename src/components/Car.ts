import * as THREE from "three";
import { Direction } from "../enums/direction";
import { coord } from "../types/roads";
import findDirection from "../helpers/findDirection";
import Junction from "./Junction";
import findDistance from "../helpers/findDistance";
import isSafePair from "../helpers/isSafePair";
const vehicleColors = [0xff1010, 0x10ff10, 0x1010ff];

export default class Car {
  carIndex: number;
  car: THREE.Group;
  path: number[];
  direction: Direction;
  targetJunction: number;
  waitingAtJunction: boolean;
  turnPhase1Completed: boolean;
  turnPhase2Completed: boolean;
  spawned: boolean;
  journeyEnded: boolean;
  public constructor(carIndex: number, path: number[], junctionCoords: coord[], color?: number) {
    this.carIndex = carIndex
    this.car = new THREE.Group();
    const actualCar = new THREE.Group();
    this.path = [];
    this.direction = Direction.North;
    this.targetJunction = -1;
    this.waitingAtJunction = false;
    this.journeyEnded = false;
    this.turnPhase1Completed = true;
    this.turnPhase2Completed = true;
    const carColor = color
      ? color
      : vehicleColors[Math.floor(Math.random() * vehicleColors.length)];

    const backWheels = new THREE.Mesh(
      new THREE.BoxGeometry(15, 15, 50),
      new THREE.MeshLambertMaterial({ color: 0x333333 })
    );
    backWheels.position.x = -18;
    actualCar.add(backWheels);

    const frontWheels = new THREE.Mesh(
      new THREE.BoxGeometry(15, 15, 50),
      new THREE.MeshLambertMaterial({ color: 0x333333 })
    );
    frontWheels.position.x = 18;
    actualCar.add(frontWheels);

    const chassisBottom = new THREE.Mesh(
      new THREE.BoxGeometry(60, 18, 40),
      new THREE.MeshLambertMaterial({ color: carColor })
    );
    chassisBottom.position.set(0, 5, 0);
    actualCar.add(chassisBottom);

    const chassisTop = new THREE.Mesh(
      new THREE.BoxGeometry(40, 12, 32),
      new THREE.MeshLambertMaterial({ color: 0xffffff })
    );
    chassisTop.position.set(-7, 20, 0);
    actualCar.add(chassisTop);

    const lightLeft = new THREE.Mesh(
      new THREE.BoxGeometry(10, 7, 11),
      new THREE.MeshLambertMaterial({ color: 0xffff00 })
    );
    lightLeft.position.set(28, 6, 10);
    actualCar.add(lightLeft);

    const lightRight = new THREE.Mesh(
      new THREE.BoxGeometry(10, 7, 10),
      new THREE.MeshLambertMaterial({ color: 0xffff00 })
    );
    lightRight.position.set(28, 6, -11);
    actualCar.add(lightRight);
    actualCar.position.z = -40;
    this.car.add(actualCar);
    this.car.scale.set(0.5, 0.5, 0.5);
    if (path.length < 2) {
      throw new Error("path must have more than one coord")
    }
    this.path = [...path].reverse();
    const firstCoord = junctionCoords[this.path[this.path.length - 1]];
    const secondCoord = junctionCoords[this.path[this.path.length - 2]];
    this.setDirection(findDirection(firstCoord, secondCoord));
    this.targetJunction = this.path[this.path.length - 2];
    this.setPosition(firstCoord);
    this.spawned = false;
    this.car.visible = false;
    this.path.pop();
  }
  public spawn(junctions: Junction[], cars: Car[]): boolean {
    if (this.spawned) {
      return true;
    }
    if(!this.isSafe(cars)) {
      return false;
    }
    console.log(this.carIndex, "spawned");
    this.car.visible = true;
    this.spawned = true;
    return true;
  }
  public getCar(): THREE.Group {
    return this.car;
  }
  private setPosition(coord: coord): void {
    this.car.position.set(coord.x, 13, coord.z)
  }
  private setDirection(direction: Direction): void {
    switch (direction) {
      case Direction.North: this.car.rotation.y = Math.PI / 2; break;
      case Direction.East: this.car.rotation.y = 0; break;
      case Direction.South: this.car.rotation.y = - Math.PI / 2; break;
      case Direction.West: this.car.rotation.y = Math.PI; break;
    }
    this.direction = direction;
  }
  public moveStraight(speed: number, junctions: Junction[], cars: Car[] = []): void {
    if (this.waitingAtJunction || !this.isSafe(cars)) {
      return;
    }
    const targetCoord = junctions[this.targetJunction].junctionCoord;
    switch (this.direction) {
      case Direction.North:
        if (this.car.position.z > targetCoord.z + 40) {
          this.car.position.z -= Math.min(this.car.position.z - (targetCoord.z + 40), speed);
        }
        break;
      case Direction.East:
        if (this.car.position.x < targetCoord.x - 40) {
          this.car.position.x += Math.min(targetCoord.x - 40 - this.car.position.x, speed);
        }
        break;
      case Direction.South:
        if (this.car.position.z < targetCoord.z - 40) {
          this.car.position.z += Math.min(targetCoord.z - 40 - this.car.position.z, speed);
        }
        break;
      case Direction.West:
        if (this.car.position.x > targetCoord.x + 40) {
          this.car.position.x -= Math.min(this.car.position.x - (targetCoord.x + 40), speed);
        }
        break;
    }
    if (Math.abs(this.car.position.x - targetCoord.x) === 40 || Math.abs(this.car.position.z - targetCoord.z) === 40) {
      this.waitingAtJunction = true;
      this.turnPhase1Completed = false;
      this.turnPhase2Completed = false;
      if (this.path.length === 1) {
        this.journeyEnded = true;
      }
    }
  }
  public turnPhase1(speed: number, junctions: Junction[]): void {
    if (!junctions[this.targetJunction].isAllowed(this.direction)) {
      return;
    }
    const targetCoord = junctions[this.targetJunction].junctionCoord
    // proceed to the centre of the junction
    switch (this.direction) {
      case Direction.North:
        if (this.car.position.z != targetCoord.z) {
          this.car.position.z += Math.min(targetCoord.z - this.car.position.z, speed)
        }
        break;
      case Direction.East:
        if (this.car.position.x != targetCoord.x) {
          this.car.position.x += Math.min(targetCoord.x - this.car.position.x, speed)
        }
        break;
      case Direction.South:
        if (this.car.position.z != targetCoord.z) {
          this.car.position.z -= Math.min(this.car.position.z - targetCoord.z, speed)
        }
        break;
      case Direction.West:
        if (this.car.position.x != targetCoord.x) {
          this.car.position.x -= Math.min(this.car.position.x - targetCoord.x, speed)
        }
        break;
    }
    if (this.car.position.x === targetCoord.x && this.car.position.z === targetCoord.z) {
      this.turnPhase1Completed = true;
    }
  }
  public turnPhase2(junctions: Junction[]): void {
    // change direction
    const firstCoord = junctions[this.path[this.path.length - 1]].junctionCoord;
    const secondCoord = junctions[this.path[this.path.length - 2]].junctionCoord;
    this.setDirection(findDirection(firstCoord, secondCoord));
    this.path.pop();
    this.targetJunction = this.path[this.path.length - 1];
    this.turnPhase2Completed = true;
    this.waitingAtJunction = false;
  }
  public isSafe(cars: Car[]) {
    for(let car of cars) {
      if(!isSafePair(this, car)) {
        return false;
      }
    }
    return true;
  }
}
